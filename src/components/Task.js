import React from 'react';
import './task.css';
//import PropTypes from 'prop-types';


class Task extends React.Component {


    styleCompleted(){
        return {
            fontSize:'20',
            color: this.props.task.done ?  'gray' : 'black',
            textDecoration: this.props.task.done ? 'line-through': 'none'
        }
    }

    
    render() {
        const { task} = this.props;
      
        return ( 
            <p  style={this.styleCompleted()} >
                {task.title} - {task.description } - {task.id}

                <input type="checkbox"  onChange={this.props.updateTask.bind(this,task.id)} />
                <button onClick={this.props.deleteTask.bind(this,task.id)}
                 style={btnDelete}  >x</button>
            </p>
        ) 
    }
}

/*
Task.prototype={
    task:PropTypes.object.isRequired
}
*/



const btnDelete={
        fontSize:'18px',
        padding:'10px 15px',
        border:'none',
        background:'geen',
        borderRadius:'50%',
        cursor:'pointer',
        color:'white'
}



export default Task;