import React,{Component} from 'react';



class TaskForm extends Component{
  
    state ={
        title:'',
        description:''
    }

    onSubmit= (e)=>{
        this.props.addTask(this.state.title,this.state.description);
        e.preventDefault();
    }


    onChange=(e)=>{
        this.setState({
            [e.target.name]:e.target.value
        });
    }

    render(){
        
        return (
            <form onSubmit={this.onSubmit} >
                <input onChange={this.onChange} 
                type="text" 
                placeholder="write a task" 
                name="title"
                value={this.state.title}
                />
                <br/>
                <br/>
                <textarea onChange={this.onChange}
                 placeholder="write a description" 
                 name="description"
                 value={this.state.description}
                 cols="30" rows="10"></textarea>
                <br/>
                <br/>
                <button type="submit" > Guardar</button>
            </form>
        )
    }
}



export default TaskForm;