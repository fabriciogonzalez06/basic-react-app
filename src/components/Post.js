import React, {Component } from 'react';



export default class Post extends Component {

    state={
        posts:[]
    }

    async componentDidMount(){
        const posts= await fetch('https://jsonplaceholder.typicode.com/posts');
        const data= await posts.json();
        console.log(data);
        this.setState({posts:data});
    }


    render(){
        return(
            <div>
                <h1>Posts</h1>

                {
                    this.state.posts.map(post=>{
                        return (
                            <li key={post.id} >  {post.title} 
                      
                             </li>
                        );
                    })
                }
           </div>
        );
    }
}