import React, { Component } from 'react';

import Task from './Task';

class Tasks extends Component {


    render() {
        return this.props.tasks.map(task => < Task deleteTask = { this.props.deleteTask }
            updateTask = { this.props.updateTask }
            task = { task }
            key = { task.id }
            />   );
        };

    }


    export default Tasks;