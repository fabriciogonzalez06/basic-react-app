import React from 'react';
import { Route,BrowserRouter as Router,Link } from 'react-router-dom'

import './App.css';
import  tasks from './sample/tasks.json';
//componentes
import TaskForm from './components/taskForm';
import Tasks from './components/Tasks';
import Post from './components/Post';
class App extends React.Component{

  state={
    tasks:tasks
  }

  addTask=(title,description)=>{
    const newTask={
      title:title,
      description:description,
      id:this.state.tasks.length
    }
    this.setState({
      tasks:[...this.state.tasks,newTask]
    });

  }

  updateTask=(id)=>{
    const newTasks= this.state.tasks.map(task=>{
      if(task.id===id){
        task.done= !task.done
      }

      return task;
    });

    this.setState({tasks:newTasks});
  }


  deleteTask=(id)=>{
    const newTasks=this.state.tasks.filter(task=> task.id!==id);
  
    this.setState({
      tasks:newTasks
    });
  }

  render(){
    return( 
      <div>

          <Router>

                  <Link to="/" >Home</Link>
                  <br/>
                  <Link to="/posts" >Posts</Link>
                  <Route exact path="/" render={()=>{
                  return ( <div>

                      <TaskForm addTask={this.addTask} />
                      <Tasks 
                      deleteTask={this.deleteTask}  
                      tasks={this.state.tasks} 
                        updateTask={this.updateTask}
                      /> 
                            </div>
                         );

                        }} >
            
                </Route>

                <Route path='/posts'  component={Post} ></Route>
          </Router>

       

      </div>
     ) 
  }
}


export default App;
